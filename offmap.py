'''
Created on May 7, 2019

@author: prateekm
'''
import plotly.plotly as py
import plotly.graph_objs as go
import pandas as pd

py.sign_in('prateekm21','z0gpkTlBstBjFGU05t9e')

df_cities = pd.read_csv("https://git.cs.vt.edu/prateekm/comboproject/raw/master/Cities.csv");
df_cities.head();

df_crewpaths = pd.read_csv("https://git.cs.vt.edu/prateekm/comboproject/raw/master/CrewPaths.csv");
df_crewpaths.head();

cities = [go.Scattergeo(
    locationmode = 'USA-states',
    lon = df_cities['Long'],
    lat = df_cities['Lat'],
    hoverinfo = 'text',
    text = df_cities['City'],
    mode = 'markers',
    marker = go.scattergeo.Marker(
        size = 3,
        color = 'rgb(213, 10, 10)',
        line = go.scattergeo.marker.Line(
            width = 3,
            color = 'rgba(68, 68, 68, 0)'
        )
    ))]

crew_paths = []
for i in range(len(df_crewpaths)):
    crew_paths.append(
        go.Scattergeo(
            locationmode = 'USA-states',
            lon = [df_crewpaths['StartLong'][i], df_crewpaths['EndLong'][i]],
            lat = [df_crewpaths['StartLat'][i], df_crewpaths['EndLat'][i]],
            mode = 'lines',
            hoverinfo = 'text',
            text = df_crewpaths['Referee'],
            line = go.scattergeo.Line(
                width = 1,
                color = 'rgb(1, 51, 105)',
            ),
        )
    )

layout1 = go.Layout(
    title = go.layout.Title(
        text = 'NFL Officiating Crews Travel'
    ),
    showlegend = False,
    geo = go.layout.Geo(
        scope = 'north america',
        projection = go.layout.geo.Projection(type = 'azimuthal equal area'),
        showland = True,
        showsubunits = True,
        landcolor = 'rgb(243, 243, 243)',
        countrycolor = 'rgb(204, 204, 204)',
    ),
)

fig1 = go.Figure(data = crew_paths +  cities, layout = layout1)
py.iplot(fig1, filename = 'crewtravels-map')