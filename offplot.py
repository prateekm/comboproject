'''
Created on May 7, 2019

@author: prateekm
'''
import plotly.plotly as py
import plotly.graph_objs as go
import pandas as pd

py.sign_in('prateekm21','z0gpkTlBstBjFGU05t9e')

df_crewsch = pd.read_csv("https://git.cs.vt.edu/prateekm/comboproject/raw/master/Data.csv");
df_crewsch.head();

crew_sch = []
for i in range(len(df_crewsch)):
    crew_sch.append(
        go.Scatter3d(
            x=df_crewsch['Referee'], y=df_crewsch['Home'], z=df_crewsch['Away'],
            marker=dict(
                size=4,
                color='rgb(1, 51, 105)',
            ),
            line=dict(
                color='rgb(213, 10, 10)',
                width=1
            ),
            name=df_crewsch['Week'][i]
        )
    )

layout2 = dict(
    title='2018 Officiating Schedule',
    scene=dict(
        xaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)',
            title='Referee Crew'
        ),
        yaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)',
            title='Home Team'
        ),
        zaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)',
            title='Away Team'
        )
    ),
)

fig2 = dict(data=crew_sch, layout=layout2)

py.iplot(fig2, filename='crewtravels-3d')